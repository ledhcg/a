function convertMetric (value, typeSource, typeTarget) {
    var value_fix;
    if (typeof value == 'number'){
        value_fix = value;
        switch(typeSource){
            case "km":
                switch(typeTarget){
                    case "km":
                        value_fix = value_fix*1;
                        break;
                    case "m":
                        value_fix = value_fix*1000;
                        break;
                    case "dm":
                        value_fix = value_fix*10000;
                        break;
                    case "cm":
                        value_fix = value_fix*100000;
                        break;
                    case "mm":
                        value_fix = value_fix*100000;
                        break;
                    default:
                        value_fix = -1;
                }
                break;
            case "m":
                switch(typeTarget){
                    case "km":
                        value_fix = value_fix*0.001;
                        break;
                    case "m":
                        value_fix = value_fix*1
                        break;
                    case "dm":
                        value_fix = value_fix*10;
                        break;
                    case "cm":
                        value_fix = value_fix*100;
                        break;
                    case "mm":
                        value_fix = value_fix*1000;
                        break;
                    default:
                        value_fix = -1;
                }
                break;
            case "dm":
                switch(typeTarget){
                    case "km":
                        value_fix = value_fix*0.0001;
                        break;
                    case "m":
                        value_fix = value_fix*0.1
                        break;
                    case "dm":
                        value_fix = value_fix*1;
                        break;
                    case "cm":
                        value_fix = value_fix*10;
                        break;
                    case "mm":
                        value_fix = value_fix*100;
                        break;

                    default:
                        value_fix = -1;
                }
                break;
            case "cm":
                switch(typeTarget){
                    case "km":
                        value_fix = value_fix*0.00001;
                        break;
                    case "m":
                        value_fix = value_fix*0.01
                        break;
                    case "dm":
                        value_fix = value_fix*0.1;
                        break;
                    case "cm":
                        value_fix = value_fix*1;
                        break;
                    case "mm":
                        value_fix = value_fix*10;
                        break;
                    default:
                        value_fix = -1;
                }
                break;
            case "mm":
                switch(typeTarget){
                    case "km":
                        value_fix = value_fix*0.000001;
                        break;
                    case "m":
                        value_fix = value_fix*0.001
                        break;
                    case "dm":
                        value_fix = value_fix*0.01;
                        break;
                    case "cm":
                        value_fix = value_fix*0.1;
                        break;
                    case "mm":
                        value_fix = value_fix*1;
                        break;
                    default:
                        value_fix = -1;
                }
                break;
            default:
                value_fix = -1;
        }
    } else {
        value_fix = -2;
    }
    
    if (value_fix === -1){
        console.log("Value must be one of the metric types: 'km', 'm', 'dm', 'cm', 'mm'.");
    } else if (value_fix === -2){
        console.log("Value must be a number.");
    } else {
        console.log(value + " " + typeSource + " = " + value_fix + " " + typeTarget);
    }
}
convertMetric(1, "km", "km");
convertMetric(1, "km", "m");
convertMetric(1, "km", "dm");
convertMetric(1, "km", "cm");
convertMetric(1, "km", "mm");

convertMetric(1, "m", "km");
convertMetric(1, "m", "m");
convertMetric(1, "m", "dm");
convertMetric(1, "m", "cm");
convertMetric(1, "m", "mm");

convertMetric(1, "dm", "km");
convertMetric(1, "dm", "m");
convertMetric(1, "dm", "dm");
convertMetric(1, "dm", "cm");
convertMetric(1, "dm", "mm");

convertMetric(1, "cm", "km");
convertMetric(1, "cm", "m");
convertMetric(1, "cm", "dm");
convertMetric(1, "cm", "cm");
convertMetric(1, "cm", "mm");

convertMetric(1, "mm", "km");
convertMetric(1, "mm", "m");
convertMetric(1, "mm", "dm");
convertMetric(1, "mm", "cm");
convertMetric(1, "mm", "mm");

convertMetric("6732", "mm", "km");
convertMetric(672.3, "dmm", "m");
convertMetric(1, "mm", "ddsm");
convertMetric(1, "mdm", "cm");
convertMetric("32732d", "mm", "mm");

